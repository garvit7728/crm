from django.apps import AppConfig


class LeadstatusConfig(AppConfig):
    name = 'leadstatus'
