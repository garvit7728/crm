from rest_framework import serializers
from .models import Lead, Stage

class LeadSerializer(serializers.ModelSerializer):
    class Meta:
        model = Lead
        fields = "__all__"
        read_only_fields = ['id', 'created_date']

    def update(self, instance, validated_data):
        instance.stage = validated_data.get('stage', instance.stage)
        instance.save()
        return instance

class StageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Stage
        fields = "__all__"
        read_only_fields = ['id', 'created_date']

    def update(self, instance, validated_data):
        instance.stage_name = validated_data.get('stage_name', instance.stage_name)
        instance.save()
        return instance