from django.db import models

# class Lead(models.Model):
#     name = models.CharField(max_length=100)
#     message = models.CharField(max_length=300)
#     created_at = models.DateTimeField(auto_now_add=True)

class Stage (models.Model):
    stage_name = models.CharField(max_length=40)

    def __str__(self):
        return self.stage_name

class Lead (models.Model):
    lead_name = models.CharField(max_length=200)
    contact_person = models.CharField(max_length=40)
    created_date = models.DateTimeField('date created', auto_now_add=True)
    stage = models.ForeignKey(Stage, on_delete= models.CASCADE)
