from django.urls import path
from . import views

urlpatterns = [
    path('api/leads', views.LeadListCreate.as_view()),
    path('api/leads/<int:pk>', views.LeadUpdate.as_view()),
    path('api/stages', views.StageListCreate.as_view()),
    path('api/stages/<int:pk>', views.StageUpdate.as_view())
]