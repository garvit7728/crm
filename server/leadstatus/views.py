from django.shortcuts import render
from .models import Lead, Stage
from .serializers import LeadSerializer, StageSerializer
from rest_framework import generics

# Create your views here.

class LeadListCreate(generics.ListCreateAPIView):
    queryset = Lead.objects.all()
    serializer_class = LeadSerializer
class LeadUpdate(generics.RetrieveUpdateDestroyAPIView):
    queryset = Lead.objects.all()
    serializer_class = LeadSerializer
class StageListCreate(generics.ListCreateAPIView):
    queryset = Stage.objects.all()
    serializer_class = StageSerializer

class StageUpdate(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = StageSerializer
    queryset = Stage.objects.all()
