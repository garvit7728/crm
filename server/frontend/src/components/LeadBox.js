import React from "react";
import PropTypes from "prop-types";

const LeadBox = ({ statestages, lead, submitStageChange }) => {
  
  return (
    <div key={lead.id}
      className=" text-left lead-box">
      <div className="lead-name">Lead: {lead.lead_name}</div>
      <div className="contact-person">Contact: {lead.contact_person}</div>
      <div className="date">Created On: {lead.created_date}</div>
      
      <div className="stage-dropdown-div">
        <form className="stage-select-form" onSubmit={e => submitStageChange(e, lead.id)}>
          <div className="d-inline">Next Stage: </div>
          <select name="stageselect" className="custom-select stage-select" defaultValue={(parseInt(lead.stage) + 1).toString()}>
            {
              statestages.map(stage => {
                if (lead.stage == stage.id) return
                return (
                  <option value={stage.id} key={stage.id}>
                    {stage.stage_name}
                  </option>
                )
              })
            }
          </select>
          <button className=" mx-auto mt-1 stage-select-button d-block btn btn-sm btn-primary" type="submit">
            submit
            </button>
        </form>
      </div>
    </div>)
}

export default LeadBox;
LeadBox.propType = {
  lead: PropTypes.object.isRequired,
  submitStageChange: PropTypes.func.isRequired,
  statestages: PropTypes.array.isRequired
};
