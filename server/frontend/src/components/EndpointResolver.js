import React, { Component } from "react";
import PropTypes from "prop-types";
class EndpointResolver extends Component {
  static propTypes = {
    endpoint1: PropTypes.string.isRequired,
    endpoint2: PropTypes.string.isRequired,
    render: PropTypes.func.isRequired,
    flip: PropTypes.bool
    };
  state = {
    data1: [],
    data2: [],
    loaded1: false,
    loaded2: false,
    placeholder1: "Loading...",
    placeholder2: "Loading...",
    flip: true
  };
  componentWillReceiveProps(props){
    if (props.flip !== this.state.flip){
      this.setState({flip: !this.state.flip})
      fetch(this.props.endpoint2)
        .then(response => {
          if (response.status !== 200) {
            return this.setState({ placeholder2: "Something went wrong" });
          }
          return response.json();
        })
        .then(data => this.setState({ data2: data, loaded2: true }));
    }
  }
  componentDidMount() {
    fetch(this.props.endpoint1)
      .then(response => {
        if (response.status !== 200) {
          return this.setState({ placeholder1: "Something went wrong" });
        }
        return response.json();
      })
      .then(data => this.setState({ data1: data, loaded1: true }));
    fetch(this.props.endpoint2)
      .then(response => {
        if (response.status !== 200) {
          return this.setState({ placeholder2: "Something went wrong" });
        }
        return response.json();
      })
      .then(data => this.setState({ data2: data, loaded2: true }));
  }
  render() {
    const { data1, data2, loaded1, loaded2, placeholder1, placeholder2 } = this.state;
    return (loaded1 && loaded2) ? this.props.render(data1, data2) : <p>{placeholder1}</p>;
  }
}
export default EndpointResolver;