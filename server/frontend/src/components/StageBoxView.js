import React from "react";
import PropTypes from "prop-types";
import LeadBox from "./LeadBox";

const StageBlock = ({ stateleads, statestages, submitStageChange }) => {
  var stages = {}
  var output = []

  stateleads.forEach(lead => {
    if (stages.hasOwnProperty(lead.stage) == false) stages[lead.stage] = []
    stages[lead.stage].push(
      <LeadBox key={lead.id}
        lead={lead}
        statestages={statestages}
        submitStageChange={submitStageChange} />
        )
  })

  statestages.forEach(stage => {
    let clname = "col" + " " + "stage" + stage.id + " " + "stage";
    output.push(
      <div key={stage.id} className={clname}>
        <h4>
          {stage.stage_name}
        </h4>
        <div className="d-flex flex-column mx-auto">
          {stages[stage.id]}
        </div>
      </div>
    )
  })

  return (
    <div className="container text-center" >
      <div className="row m-2">
        {output}
      </div>
    </div >
  )
}
export default StageBlock;
StageBlock.propType = {
  stateleads: PropTypes.array.isRequired,
  statestages: PropTypes.array.isRequired,
  submitStageChange: PropTypes.func.isRequired
}

