import React, { Component } from "react";
import PropTypes from "prop-types";
import StageBlock from "./StageBoxView";
import axios from 'axios';
axios.defaults.xsrfCookieName = 'csrftoken'
axios.defaults.xsrfHeaderName = 'X-CSRFToken'

export default class StageBox extends Component {

  static propTypes = {
    leads: PropTypes.array.isRequired,
    stages: PropTypes.array.isRequired
  };
  state = {
    leads: [],
    stages: [],
    loaded: false,
    placeholder: "Loading..."
  }
  componentWillReceiveProps(props) {
    // console.log('chalgya',props.leads)
    this.setLead(props)
  }
  componentDidMount() {
    // console.log(this.props)
    this.setState({ stages: this.props.stages })

    this.setLead(this.props)

    this.setState({ loaded: true })
  }
  setLead(props) {
    let leads = props.leads.map(lead => {
      var date = new Date(lead.created_date)
      date = date.getDate() + "/" + date.getMonth() + "/" + date.getFullYear()

      lead.created_date = date

      return lead
    })
    this.setState({ leads: leads })
  }
  submitStageChange(ev, leadid) {
    ev.preventDefault();
    let putdata = null
    let stageid = ev.target.stageselect.value
    let leads = this.state.leads.filter(lead => {
      if (lead.id == leadid) {
        lead.stage = stageid
        putdata = lead
      }
      return lead
    })
    let url = '/api/leads/' + leadid
    var csrftoken = getCookie('csrftoken');
    axios.put(url, putdata).then(response => {
      this.setState({
        ...this.state,
        leads
      })
    })
  }
  render() {

    return this.state.loaded ? (
      <StageBlock
        stateleads={this.state.leads}
        statestages={this.state.stages}
        submitStageChange={(e, leadid) => this.submitStageChange(e, leadid)}
      />
    ) : <p>{this.state.placeholder}</p>
  }
}

function getCookie(name) {
  var cookieValue = null;
  if (document.cookie && document.cookie !== '') {
    var cookies = document.cookie.split(';');
    for (var i = 0; i < cookies.length; i++) {
      var cookie = jQuery.trim(cookies[i]);
      if (cookie.substring(0, name.length + 1) === (name + '=')) {
        cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
        break;
      }
    }
  }
  return cookieValue;
}

