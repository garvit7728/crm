import React, { Component } from "react";
import PropTypes from "prop-types";
import axios from "axios";

export default class LeadBoxInsert extends Component {

  static propTypes = {
    statestages: PropTypes.array.isRequired,
    leadendpoint: PropTypes.string.isRequired,
    triggerRender: PropTypes.func.isRequired
  }
  state = {
    flip: true
  }
  submitLeadInsert(e) {
    e.preventDefault();
    let lead_name = e.target.lead_name.value
    let contact_person = e.target.contact_person.value
    let stage = e.target.stageselect.value
    if (stage == "x") return

    let postData = {
      lead_name: lead_name,
      contact_person: contact_person,
      stage: stage
    }

    axios.post(this.props.leadendpoint, postData)
      .then(response => {
        // console.log(response)
        if (response.status == 201) {
          this.setState({flip: !this.state.flip})
          this.props.triggerRender(this.state.flip)
        } else {
          alert("Something went wrong")
        }
      })
  }

  render() {
    let statestages = this.props.statestages
    return (
      <div className="lead-box">
        <form className="stage-select-form" onSubmit={e => this.submitLeadInsert(e)}>
          
          <div className="lead-name my-1">
          <input name="lead_name" placeholder="Lead" />
          </div>
          <div className="contact-name my-1">            
            <input name="contact_person" placeholder="Contact Person" />
          </div>

          <div className="stage-dropdown-div">
            <select name="stageselect" className="custom-select stage-select">
              <option value="x" key="x">Choose a Stage</option>
              {
                statestages.map(stage => {
                  return (
                    <option value={stage.id} key={stage.id}>
                      {stage.stage_name}
                    </option>
                  )
                })
              }
            </select>
            <button className=" m-1 stage-select-button btn d-block btn-sm btn-primary" type="submit">
              Submit
            </button>
          </div>
        </form>
      </div>)
  }
}


