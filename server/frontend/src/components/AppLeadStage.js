import React from "react";
import ReactDOM from "react-dom";
import DataProvider from "./DataProvider";
import EndpointResolver from "./EndpointResolver";
import StageBox from "./StageBox";
import LeadBoxInsert from "./LeadBoxInsert";

const wrapperLeadInsert = document.getElementById("appleadinsert");
const wrapperStageBox = document.getElementById("appstagebox");

let flip = false
const AppLeadInsert = () => (
  <DataProvider
    endpoint="/api/stages"
    render={stages =>
      <LeadBoxInsert
        statestages={stages}
        leadendpoint="/api/leads"
        triggerRender={ (flip) =>
          ReactDOM.render(<AppStageBox flip={flip} />, wrapperStageBox)
        }
      />
    }
  />
);

const AppStageBox = ({flip}) => (
  <EndpointResolver
    endpoint1="/api/stages"
    endpoint2="/api/leads"
    flip={flip}
    render={(stages, leads) =>
      <StageBox
        leads={leads}
        stages={stages}
      />
    }
  />
);

wrapperLeadInsert ? ReactDOM.render(<AppLeadInsert />, wrapperLeadInsert) : null;
wrapperStageBox ? ReactDOM.render(<AppStageBox flip={flip} />, wrapperStageBox) : null;
